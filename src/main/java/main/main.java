package main;

import static spark.Spark.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;

public class main {
	
	static File file = new File("file.json");
	static JSONParser parser = new JSONParser();
	static Bill bill;
	public static pdf pdf= new pdf();

	
	public static void main(String[] args) {

		//http://localhost:8081/ //For Open de IU
    	port(8081);
        get("/", (request, response) ->
        {
        	Map<String, Object> model = new HashMap<>();
        	model.put("Message","Bienvenido al Facturador");
        	return new VelocityTemplateEngine().render(new ModelAndView(model, "velocity/main.vm"));
        });
        
        get("/api/getRate", (request, response) ->
        {
        	Map<String, Object> model = new HashMap<>();
        	String number = request.queryParams("number");
	  		String date = request.queryParams("date");
        	model.put("Title","Factura");
        	model.put("number",number); 
        	model.put("date",date);
        	
        	String output="";
        	String UrlString = "http://localhost:8080/getRate/".concat(number).concat("/date/").concat(date);

        	URL url = new URL(UrlString);//your url i.e fetch data from .
        	//URL url = new URL("http://localhost:8080/getRate");//your url i.e fetch data from .
    		
    		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
    		connection.setRequestMethod("GET");
    		connection.setRequestProperty("Accept", "application/json");
    		
    		if (connection.getResponseCode() != 200) {
    		    throw new RuntimeException("Failed : HTTP Error code : "
    		            + connection.getResponseCode());
    		}
    		
    		InputStreamReader in = new InputStreamReader(connection.getInputStream());
    		BufferedReader reader = new BufferedReader(in);
    		FileWriter fileWriter=new FileWriter(file);
    		BufferedWriter writer = new BufferedWriter(fileWriter);

    		while ((output = reader.readLine()) != null) {
    		    System.out.println(output);
    		    writer.write(output);
    		}
    		
    		writer.flush();
    		fileWriter.close();
    		writer.close();
    		connection.disconnect();
    		
    		extractData();
        	model.put("totalCost",bill.getcost());

    		pdf.crearPDF(bill);

        	return new VelocityTemplateEngine().render(new ModelAndView(model, "velocity/billview.vm"));
        });
	}
	private static void saveDataIntoObject(JSONObject cdr) 
    {
        //Get employee first name
        long numeroTelefonico = (long) cdr.get("phoneNumber");    
        System.out.println(numeroTelefonico);
        String mes = (String) cdr.get("month");    
        System.out.println(mes);
        double montoMes = (double) cdr.get("cost");    
        System.out.println(montoMes);
        
        bill = new Bill(numeroTelefonico,mes,montoMes);
    }
	
	private static void extractData() 
    {
		try {
			Object obj = parser.parse(new FileReader("file.json"));
			JSONObject jsonFile = (JSONObject) obj;
			saveDataIntoObject(jsonFile);
			//ExtractEachRegister(jsonFile);
	        
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }

	private static void ExtractEachRegister(JSONArray jsonFile) {
		jsonFile.forEach( cdr -> saveDataIntoObject( (JSONObject) cdr ));
	}
}
