package main;

public class Bill {
	private long phoneNumber;
	private double cost; 
	private String month;
	
	public Bill(long phoneNumber,String month,double cost){
		this.phoneNumber = phoneNumber;
		this.month=month;
		this.cost = cost;
	}

	public long getphoneNumber() {
		return phoneNumber;
	}

	public void setphoneNumber(Integer phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public double getcost() {
		return cost;
	}

	public void setcost(double cost) {
		this.cost = cost;
	}

	public String getmonth() {
		return month;
	}

	public void setmonth(String month) {
		this.month = month;
	}

}