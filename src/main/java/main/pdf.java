package main;

import java.io.FileOutputStream;
import java.util.Date;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class pdf {

    private static String FILE = "Factura.pdf";
    
    private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,
            Font.BOLD);
    private static Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.NORMAL, BaseColor.RED);
    private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16,
            Font.BOLD);
    private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.BOLD);
    
    public pdf()
    {
    }

    public void crearPDF(Bill bill) {
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(FILE));
            
            document.open();
            addMetaData(document);
            addContent(document,bill);
            document.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void addMetaData(Document document) {
        document.addTitle("Factura Costo Telefonico");
        document.addSubject("Using iText");
        document.addKeywords("Java, PDF, iText");
        document.addAuthor("Carlos Luna - Diego Viscarra - Romel Saavedra");
        document.addCreator("Grupo 2");
    }


    private static void addContent(Document document,Bill bill) throws DocumentException {
    	Paragraph preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("Factura total de costos de llamada", catFont));
        addEmptyLine(preface, 1);
     
        createTable(preface,bill);
        addEmptyLine(preface, 5);
        
        preface.add(new Paragraph(
                "Fecha del reporte: "+ new Date(), smallBold));
        
        document.add(preface);

    }

    private static void createTable(Paragraph paragraph,Bill bill)
            throws BadElementException {
        PdfPTable table = new PdfPTable(3);
        PdfPCell c1 = new PdfPCell(new Phrase("Numero telefonico del propietario"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Mes"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Costo total del mes Bs"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);
        table.setHeaderRows(1);
        
        String numeroTelefonico=String.valueOf(bill.getphoneNumber());
        table.addCell(numeroTelefonico);
        table.addCell(bill.getmonth());
        String montoMes=String.valueOf(bill.getcost());
        table.addCell(montoMes);
        
        paragraph.add(table);

    }

    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
}